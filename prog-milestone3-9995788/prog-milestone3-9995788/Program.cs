﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9995788
{
    class Program
    {
        public static void Main(string[] args)
        {
            int Menu;
            string PizzaSelect;
            int i;
            int x;
            var Flavours = new List<string> { "Hawaiian", "Vegertarian", "Cheesy", "Meat Lovers" };
            var Order = new List<Tuple<string, string, double>>();
            double cost = 0;
            string owing;
            int payment = 0;
            double cash = 0;

            List<Tuple<string, double>> Size = new List<Tuple<string, double>>();
            Size.Add(Tuple.Create("Small", 5.85));
            Size.Add(Tuple.Create("Medium", 10.35));
            Size.Add(Tuple.Create("Large", 15.20));

            List<Tuple<string, double>> drink = new List<Tuple<string, double>>();
            drink.Add(Tuple.Create("Soda", 2.55));
            drink.Add(Tuple.Create("Juice", 3.20));
            drink.Add(Tuple.Create("Coffee", 5.00));

        welcomeScreen:
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Hello valued customer and welcome to our pizza ordering system");
            //Console.WriteLine(" press any key to continue");
            //Console.ReadLine();
            Client.CustomerDetails();

        menuScreen:

            Console.Clear();
            Pizza.display();
            Console.WriteLine();
            Console.WriteLine("Menu Select:");
            Console.WriteLine("1. Pizza");
            Console.WriteLine("2. Drinks");
            Console.WriteLine("3. Place Order");
            Console.WriteLine();

            if (int.TryParse(Console.ReadLine(), out x))
            {
                switch (x)
                {
                    case 1:
                    Pizzastart:
                        Console.Clear();
                        Console.WriteLine("");
                        Console.WriteLine("Here is your current order");
                        Pizza.display();
                        for (i = 0; i < Flavours.Count; i++)
                        {
                            Console.WriteLine((i + 1) + ". " + Flavours[i]);

                        }
                        Console.WriteLine("");
                        Console.WriteLine((i + 1) + ". Return to menu");

                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            if (x == i + 1)
                            {
                                goto menuScreen;
                            }
                            else if (x > i + 1)
                            {
                                Console.WriteLine("");
                                Console.WriteLine("Oops, please try again");
                                Console.ReadLine();
                                goto Pizzastart;
                            }
                            else
                            {
                                PizzaSelect = Flavours[x - 1];
                                Console.Clear();
                                Pizza.display();
                                for (i = 0; i < Size.Count; i++)
                                {
                                    Console.WriteLine((i + 1) + ". " + Size[i].Item1);
                                }
                                Console.WriteLine("");
                                Console.WriteLine((i + 1) + ". return to menu");

                                if (int.TryParse(Console.ReadLine(), out x))
                                {
                                    if (x == i + 1)
                                    {
                                        goto menuScreen;
                                    }
                                    else if (x > i + 1)
                                    {
                                        Console.WriteLine("Oops, please try again");
                                        Console.ReadLine();
                                        goto Pizzastart;
                                    }
                                    else
                                    {
                                        Order.Add(Tuple.Create(PizzaSelect, Size[x - 1].Item1, Size[x - 1].Item2));
                                        Pizza.order.Add(Tuple.Create(PizzaSelect, Size[x - 1].Item1, Size[x - 1].Item2));
                                    }
                                    goto Pizzastart;
                                }
                                else
                                {
                                    Console.WriteLine("Oops, please try again");
                                    Console.ReadLine();
                                    goto Pizzastart;
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("Oops, please try again");
                            Console.ReadLine();
                            goto Pizzastart;
                        }

                    case 2:
                    Drinkstart:
                        Console.Clear();

                        Pizza.display();
                        for (i = 0; i < drink.Count; i++)
                        {
                            Console.WriteLine((i + 1) + ". " + drink[i].Item1);
                        }
                        Console.WriteLine("");
                        Console.WriteLine((i + 1) + ". Return to menu");

                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            if (x == i + 1)
                            {
                                goto menuScreen;
                            }
                            else if (x > i + 1)
                            {
                                Console.WriteLine("Oops, please try again");
                                Console.ReadLine();
                                goto Drinkstart;
                            }
                            else
                            {
                                Order.Add(Tuple.Create("Drink     ", drink[x - 1].Item1, drink[x - 1].Item2));
                                Pizza.order.Add(Tuple.Create("Drink     ", drink[x - 1].Item1, drink[x - 1].Item2));
                            }
                            goto Drinkstart;
                        }
                        else
                        {
                            Console.WriteLine("Oops, please try again");
                            Console.ReadLine();
                            goto Drinkstart;
                        }

                    case 3:
                    Confirmation:

                        Console.Clear();
                        Pizza.display();
                        Console.WriteLine("Is this the correct order?");
                        Console.WriteLine("1. yes");
                        Console.WriteLine("2. no");

                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            switch (x)
                            {
                                case 1:
                                    Console.Clear();
                                    for (i = 0; i < Order.Count; i++)
                                    {
                                        owing = String.Format("{0:C}", Order[i].Item3);
                                        Console.WriteLine(Order[i].Item1 + "    " + Order[i].Item2 + "    " + owing);
                                        cost = cost + Order[i].Item3;
                                    }
                                    owing = String.Format("{0:C}", cost);
                                    Console.WriteLine("");
                                    Console.WriteLine("cost:   " + cost);
                                    Console.WriteLine("");

                            paying:
                                    Console.WriteLine("Will you be using cash or credit card?");
                                    Console.WriteLine("1. cash");
                                    Console.WriteLine("2. credit");
                                    payment = int.Parse(Console.ReadLine());

                                    if (payment == 1)
                                    {
                                        Console.WriteLine("Please provide the ammount of cash you will pay with");
                                        cash = double.Parse(Console.ReadLine());

                                        if (cash >= cost)
                                        {
                                            cash = cost - cash;
                                            Console.WriteLine("Thank you for your payment! Your change will be $" + cash);
                                            Console.ReadLine();
                                        }
                                        else if (cash < cost)
                                        {
                                            Console.WriteLine("I'm sorry, but that is not enough to pay your bill");
                                            goto paying;
                                        }
                                        else
                                        {
                                            Console.WriteLine("Oops, please try again");
                                            Console.ReadLine();
                                            goto paying;
                                        }
                                    }
                                    else if (payment == 2)
                                    {
                                        Console.WriteLine("Please enter your card number"); //Dear lord, dear god please do not use real credit card number
                                        int CardNum = int.Parse(Console.ReadLine());

                                        Console.WriteLine("");
                                        Console.WriteLine("Is this the correct card number?   " + CardNum);
                                        Console.WriteLine("1. yes");
                                        Console.WriteLine("2. no");
                                        var card = int.Parse(Console.ReadLine());

                                        if (card == 1)
                                        {
                                            Console.WriteLine("Thank you for your payment!!");
                                            Console.ReadLine();
                                        }
                                        else if (card == 2)
                                        {
                                            Console.WriteLine("Please try again");
                                            goto paying;
                                        }
                                        else
                                        {                                         
                                            Console.WriteLine("Oops, please try again");
                                            Console.ReadLine();
                                            goto paying;
                                        }

                                    }
                                    Environment.Exit(0);
                                    break;

                                case 2:
                                    goto menuScreen;
                            }
                        }
                        break;


                }
            }


        }

        public static class Pizza
        {
            public static List<Tuple<string, string, double>> order = new List<Tuple<string, string, double>>();

            public static void display()
            {
                int i;
                double price = 0;
                string owing;

                Console.WriteLine("Name- " + Client.name);
                Console.WriteLine("Phone Number- " + Client.number);

                Console.WriteLine("Here is your order...");

                for (i = 0; i < order.Count; i++)
                {
                    owing = String.Format("{0:C}", order[i].Item3);
                    Console.WriteLine(order[i].Item1, order[i].Item2, owing);
                    price = price + order[i].Item3;
                }

                owing = String.Format("{0:C}", price);
                Console.WriteLine("");
                Console.WriteLine("The cost will be $" + price);
            }

        }

        public static class Client
        {
            public static string name;
            public static string number;

            public static void CustomerDetails()
            {
                int x;

            start:

                Console.WriteLine("");
                Console.WriteLine("Please input your name");
                name = Console.ReadLine();

                Console.Clear();

                Console.WriteLine("");
                Console.WriteLine("Please input your phone number");
                number = Console.ReadLine();




            }
        }


      
    }

}
